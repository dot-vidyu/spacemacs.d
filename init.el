;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '("~/.spacemacs.d/layers/")

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   `(rust
     swift
     ;; (,(when (eq system-type 'darwin) 'selectric))

     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;; Languages
     (java :variables java-backend 'meghanada)
     cucumber
     clojure
     ;; (java :variables java-backend 'eclim
     ;;       eclim-eclipse-dirs '("/Applications/Eclipse.app/Contents/Eclipse/")
     ;;       eclim-executable "/Applications/Eclipse.app/Contents/Eclipse/eclim")
     shell-scripts
     dap
     lua
     haskell elixir emacs-lisp
     nixos
     sql
     ;; php
     python
     (javascript :variables javascript-backend 'tern)
     ;; typescript
     (typescript :variables
                 typescript-linter 'eslint
                 typescript-backend 'lsp
                 ;; typescript-lsp-linter nil
                 )
     react html (node :variables node-add-modules-path t)
     (vue :variables vue-backend 'lsp)
     (lsp :variables
          lsp-ui-sideline-enable nil
          lsp-headerline-breadcrumb-enable nil
          lsp-lens-enable t
          )
     gtags
     tern
     (auto-completion :variables
                      auto-completion-enable-help-tooltip t)
     syntax-checking
     ;; File formats
     vimscript yaml csv nginx systemd markdown docker
     (org :variables
          org-enable-org-journal-support t
          org-enable-reveal-js-support t
          org-enable-jira-support t
          org-enable-github-support t
          org-enable-bootstrap-support t
          org-enable-roam-support t
          org-enable-verb-support t
          org-enable-appear-support t
          org-enable-asciidoc-support t
          org-enable-valign t
          jiralib-url "https://jira.meinestadt.de"
          )
     spacemacs-org
     ;; google-calendar
     ;; Tools
     ;; confluence
     (ranger :variables ranger-override-dired 'ranger ranger-show-preview t)
     helm ibuffer
     git version-control
     restclient
     ;; mu4e
     (shell :variables
            shell-default-height 30
            ;; shell-default-term-shell "/usr/bin/fish"
            shell-default-term-shell "/usr/local/bin/fish"
            shell-default-position 'bottom)
     (spell-checking ;; :variables
      ;; spell-checking-enable-by-default nil
      )
     multiple-cursors
     ;; UI
     ;; emoji
     unicode-fonts
     ;; neotree
     (treemacs :variables
               treemacs-use-filewatch-mode t
               treemacs-use-follow-mode t
               treemacs-use-git-mode 'deferred
               )
     spacemacs-layouts
     colors
     ;; themes-megapack
     local
     ,@(if (eq system-type 'darwin)
           '((osx :variables
                  ;; osx-command-as       'hyper
                  osx-option-as        'meta
                  ;; osx-control-as       'control
                  ;; osx-function-as      nil
                  ;; osx-right-command-as 'left
                  osx-right-option-as  'none
                  ;; osx-right-control-as 'left
                  ;; osx-swap-option-and-command nil
                  ))
         )
     )

   ;; List of additional packages that will be installed without being wrapped
   ;; in a layer (generally the packages are installed only and should still be
   ;; loaded using load/require/use-package in the user-config section below in
   ;; this file). If you need some configuration for these packages, then
   ;; consider creating a layer. You can also put the configuration in
   ;; `dotspacemacs/user-config'. To use a local version of a package, use the
   ;; `:location' property: '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages '(
                                      direnv
                                      sqlite3
                                      kaolin-themes
                                      exec-path-from-shell
                                      reason-mode
                                      plantuml-mode
                                      geben
                                      doom-themes
                                      ;; lab-themes
                                      (lsp-tailwindcss :location (recipe
                                                                  :fetcher github
                                                                  :repo "merrickluo/lsp-tailwindcss" 
                                                                  ))
                                      ;; lsp-tailwindcss
                                      )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need to
   ;; compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;;
   ;; WARNING: pdumper does not work with Native Compilation, so it's disabled
   ;; regardless of the following setting when native compilation is in effect.
   ;;
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
   ;; (default (format "spacemacs-%s.pdmp" emacs-version))
   dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; Set `read-process-output-max' when startup finishes.
   ;; This defines how much data is read from a foreign process.
   ;; Setting this >= 1 MB should increase performance for lsp servers
   ;; in emacs 27.
   ;; (default (* 1024 1024))
   dotspacemacs-read-process-output-max (* 1024 1024)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. Spacelpa is currently in
   ;; experimental state please use only for testing purposes.
   ;; (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim

   ;; If non-nil show the version string in the Spacemacs buffer. It will
   ;; appear as (spacemacs version)@(emacs version)
   ;; (default t)
   dotspacemacs-startup-buffer-show-version t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   ;; The exceptional case is `recents-by-project', where list-type must be a
   ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
   ;; number is the project limit and the second the limit on the recent files
   ;; within a project.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Show numbers before the startup list lines. (default t)
   dotspacemacs-show-startup-list-numbers t

   ;; The minimum delay in seconds between number key presses. (default 0.4)
   dotspacemacs-startup-buffer-multi-digit-delay 0.4

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'org-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'org-mode

   ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
   ;; *scratch* buffer will be saved and restored automatically.
   dotspacemacs-scratch-buffer-persistent nil

   ;; If non-nil, `kill-buffer' on *scratch* buffer
   ;; will bury it instead of killing.
   dotspacemacs-scratch-buffer-unkillable nil

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(
                         doom-xcode
                         kaolin-galaxy
                         modus-vivendi
                         modus-operandi
                         kaolin-aurora
                         ;; (kaolin-aurora :location (recipe :fetcher github :repo "ogdenwebb/emacs-kaolin-themes"))
                         ;; (almost-mono-white :location (recipe :fetcher github :repo "cryon/almost-mono-themes"))
                         doom-nord-light
                         ;; (flucui-dark :location (recipe :fetcher github :repo "MetroWind/flucui-theme"))
                         ;; (flucui-light :location (recipe :fetcher github :repo "MetroWind/flucui-theme"))
                         doom-one-light
                         spacemacs-dark
                         )
   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(doom
                                  :separator none
                                  :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font or prioritized list of fonts. The `:size' can be specified as
   ;; a non-negative integer (pixel size), or a floating-point (point size).
   ;; Point size is recommended, because it's device independent. (default 10.0)
   ;; dotspacemacs-default-font '("SauceCodePro Nerd Font" :size 12.0 :weight normal :width normal)
   dotspacemacs-default-font '(
                               ("Intel One Mono" :size 12.0 :weight normal :width normal :powerline-scale 1.2)
                               ("InconsolataLGC Nerd Font Mono" :size 12.0 :weight normal :width normal :powerline-scale 1.2)
                               ("Fura Code Nerd Font" :size 12.0 :weight normal :width normal :powerline-scale 1.2)
                               ("Fira Code" :size 12.0 :weight normal :width normal :powerline-scale 1.2)
                               ("Inconsolata" :size 12.0 :weight normal :width normal :powerline-scale 1.2)
                               )

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
   ;; Thus M-RET should work as leader key in both GUI and terminal modes.
   ;; C-M-m also should work in terminal mode, but not in GUI mode.
   dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab t

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 80

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Show the scroll bar while scrolling. The auto hide time can be configured
   ;; by setting this variable to a number. (default t)
   dotspacemacs-scroll-bar-while-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but only visual lines are counted. For example, folded lines will not be
   ;; counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
   ;; `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil smartparens-mode will be enabled in programming modes.
   ;; (default t)
   dotspacemacs-activate-smartparens-mode t

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; If nil then Spacemacs uses default `frame-title-format' to avoid
   ;; performance issues, instead of calculating the frame title by
   ;; `spacemacs/title-prepare' all the time.
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Show trailing whitespace (default t)
   dotspacemacs-show-trailing-whitespace t

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil

   ;; If non-nil activate `clean-aindent-mode' which tries to correct
   ;; virtual indentation of simple modes. This can interfere with mode specific
   ;; indent handling like has been reported for `go-mode'.
   ;; If it does deactivate it here.
   ;; (default t)
   dotspacemacs-use-clean-aindent-mode t

   ;; Accept SPC as y for prompts if non-nil. (default nil)
   dotspacemacs-use-SPC-as-y nil

   ;; If non-nil shift your number row to match the entered keyboard layout
   ;; (only in insert state). Currently supported keyboard layouts are:
   ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
   ;; New layouts can be added in `spacemacs-editing' layer.
   ;; (default nil)
   dotspacemacs-swap-number-row nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs t

   ;; If nil the home buffer shows the full path of agenda items
   ;; and todos. If non-nil only the file name is shown.
   dotspacemacs-home-shorten-agenda-source nil

   ;; If non-nil then byte-compile some of Spacemacs files.
   dotspacemacs-byte-compile nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env)
  (setenv "PATH" (concat (getenv "PATH") ":" dotspacemacs-directory "node_modules/.bin/"))
  (setenv "PATH" (concat (getenv "PATH") ":" "/usr/local/bin/"))
  (add-to-list 'exec-path (concat dotspacemacs-directory "node_modules/.bin/"))
  ;; (setenv "SSH_AUTH_SOCK" "/Users/user/.gnupg/S.gpg-agent.ssh")
  )

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  ;; (setq configuration-layer-elpa-archives '(("melpa" . "melpa.org/packages/")
  ;;             "                              ("org . "orgmode.org/elpa/") ("gnu" . "elpa.gnu.org/packages/")))

  (setq org-roam-v2-ack t)

  ;; (setq org-gcal-client-id "178173183204-qqpthrkjjqaqq7hvs8pceicjphsihp3h.apps.googleusercontent.com"
  ;;       org-gcal-client-secret "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")
  ;; (setq org-gcal-file-alist '(("vidyu.krastev@meinestadt.de" . "~/.gtd/calendar.org")))

  (setq ranger-enter-with-minus 'ranger)
  ;; modus-themes config
  ;; Choose to render more code constructs in slanted text (italics).  The
  ;; default, shown below, is to not use italics, unless it is absolutely
  ;; necessary.
  (setq modus-operandi-theme-slanted-constructs t)

  (setq modus-vivendi-theme-slanted-constructs t)

  ;; Make the fringes visible.  This renders them in a different
  ;; background than the main buffer.
  (setq modus-operandi-theme-visible-fringes nil)

  (setq modus-vivendi-theme-visible-fringes nil)

  ;; Opt to display some additional code constructs in bold.  The default,
  ;; shown below, is to use bold weight only where necessary.
  (setq modus-operandi-theme-bold-constructs t)

  (setq modus-vivendi-theme-bold-constructs t)

  ;; Use proportionately-spaced fonts (variable-pitch) for headings.  The
  ;; default is to use whatever font the user has selected, typically a
  ;; monospaced typeface.
  (setq modus-operandi-theme-proportional-fonts t)

  (setq modus-vivendi-theme-proportional-fonts t)

  ;; Whether headings should be scaled or have the same height as body
  ;; text.  The default is to keep everything the same as the base size.
  (setq modus-operandi-theme-scale-headings t)

  (setq modus-vivendi-theme-scale-headings t)

  (setq-default helm-buffer-max-length nil)

  ;; end modus theme config


  (defconst dotfiles-dir
    (file-name-directory
     (or (buffer-file-name) load-file-name))
    "Base path for customised Emacs configuration.")
  (setq-default
   shell-file-name "/bin/sh"
   flycheck-disabled-checkers (quote (javascript-jscs javascript-jshint))
   ;; browse-url-browser-function (quote browse-url-default-macosx-browser)
   js-indent-level 2
   ;; js-expr-indent-offset -2
   js-switch-indent-offset 2
   js2-basic-offset 2)
  ;; (setenv "PATH" (concat (getenv "PATH") ":" dotspacemacs-directory "~/.composer/vendor/bin/"))
  ;; (add-to-list 'exec-path (concat dotspacemacs-directory "~/.composer/vendor/bin/"))
  ;; Prevent Custom from dumping its local settings into this file.
  (setq-default custom-file (concat dotspacemacs-directory "custom.el"))

  (setq-default
   doom-modeline-height 20
   doom-modeline-persp-name nil
   doom-modeline-buffer-file-name-style 'relative-to-project
   doom-modeline-vcs-max-length 24)

  (load-file custom-file)
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

  (setq backup-directory-alist `((".*" . ,temporary-file-directory)))
  (setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))
  (setq create-lockfiles nil)

  (setq valign-fancy-bar t)

  (setq helm-buffer-max-length nil)

  ;; (setq default-frame-alist '((undecorated . t)))
  ;; (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  ;; (add-to-list 'default-frame-alist '(ns-appearance . light))
  ;; (add-to-list 'default-frame-alist '(drag-internal-border . 1))
  ;; (add-to-list 'default-frame-alist '(internal-border-width . 5))
  ;; (setq frame-resize-pixelwise t)

  ;; for the completion in e-shell
  (setq helm-show-completion-display-function #'helm-show-completion-default-display-function)

  (defun helm-buferm-max-lenght-to-nil ()
    "Set Helm Buffer max length no nil"
    (interactive)
    (setq helm-buffer-max-length nil)
    )

  ;; direnv
  (use-package direnv
    :demand t
    :config
    (direnv-mode +1))

  (use-package lsp-tailwindcss
    :init
    (setq lsp-tailwindcss-add-on-mode t)
    (setq lsp-tailwindcss-major-modes '(rjsx-mode web-mode html-mode css-mode typescript-mode vue-mode))
    )

  ;; (use-package lsp-typescript
  ;;   :init
  ;;   (setq lsp-typescript-major-modes '(rjsx-mode web-mode html-mode css-mode typescript-mode vue-mode))
  ;;   )
  ;; (setq lsp-tailwindcss-add-on-mode t)
  ;; (use-package lsp-tailwindcss)

  (with-eval-after-load "ispell"
    (setq ispell-program-name "hunspell")
    ;; ispell-set-spellchecker-params has to be called
    ;; before ispell-hunspell-add-multi-dic will work
    (ispell-set-spellchecker-params)
    (ispell-hunspell-add-multi-dic "de_DE,en_GB")
    (setq ispell-dictionary "de_DE,en_GB"))

  ;; (setq meghanada-java-path (expand-file-name "bin/java" "/Users/user/.nix-profile"))

  ;; (eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
  (evil-define-key 'normal prog-mode-map
    (kbd "zn") 'hs-hide-level)

  ;; fly-spell skip-region
  (add-to-list 'ispell-skip-region-alist '("^flyspell-disable" . "^flyspell-disable"))


  ;; typescript tsx lint
  ;; (add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
  (with-eval-after-load 'flycheck
    (flycheck-add-mode 'javascript-eslint 'typescript-tsx-mode)
    (lsp-flycheck-add-mode 'vue-mode)
    ;; (flycheck-add-mode 'lsp 'vue-mode)
    (flycheck-define-checker scss-stylelint
      "A SCSS syntax and style checker using stylelint.

See URL `http://stylelint.io/'."
      :command ("stylelint"
                (eval flycheck-stylelint-args)
                ;; "--syntax" "scss"
                (option-flag "--quiet" flycheck-stylelint-quiet)
                (config-file "--config" flycheck-stylelintrc))
      :standard-input t
      :error-parser flycheck-parse-stylelint
      :predicate flycheck-buffer-nonempty-p
      :modes (scss-mode))
    )

  ;; (setq lsp-diagnostic-package :none)
  (setq ediff-diff-program "~/.local/bin/diff-en")
  (setq split-width-threshold 180)
  (setq split-height-threshold nil)
  ; js indentation
  (advice-add 'js--multi-line-declaration-indentation :around (lambda (orig-fun &rest args) nil))
  (add-hook 'after-init-hook #'global-emojify-mode)
  ;; (set-fontset-font "fontset-default" '(#o21142 . #o21142) "Fira Code")
  (set-fontset-font "fontset-default" 'unicode "Fira Code")
  (defun set-pretty-symbols-alist ()
    (setq-local prettify-symbols-alist '(
                                         ("->" . ?→)
                                         ("function" . ?λ)
                                         ("null" . ?∅)
                                         ("&&" . ?∧)
                                         ("||" . ?∨)
                                         ("!==" . ?≢)
                                         ("===" . ?≣)
                                         ("=>" . ?⇒)
                                         (">=" . ?≥)
                                         ("<=" . ?≤)))
    )

  (add-hook 'tide-mode-hook 'prettier-js-mode)
  ;; (add-hook 'js2-mode-hook 'prettier-js-mode)
  (add-hook 'vue-mode-hook 'prettier-js-mode)
  (add-hook 'js2-mode-hook 'set-pretty-symbols-alist)
  (add-hook 'js2-mode-hook 'lsp)
  (add-hook 'tide-mode-hook 'set-pretty-symbols-alist)
  (add-hook 'typescript-mode-hook 'set-pretty-symbols-alist)
  (add-hook 'typescript-tsx-mode-hook 'set-pretty-symbols-alist)
  (add-hook 'vue-mode-hook (lambda ()
                             (set-pretty-symbols-alist)
                             (prettify-symbols-mode +1)))
  ;; (add-hook 'vue-mode-hook (prettify-symbols-mode +1))
  ;; (add-hook 'vue-mode-hook 'set-pretty-symbols-alist)

  ;; (add-hook 'php-mode-hook
  ;;           (lambda ()
  ;;             (lsp)
  ;;             (set-pretty-symbols-alist)
  ;;             (prettify-symbols-mode +1)
  ;;             ))

  (add-hook 'js2-mode-hook 'flyspell-prog-mode)
  (add-hook 'typescript-mode-hook 'flyspell-prog-mode)
  (add-hook 'vue-mode-hook 'flyspell-prog-mode)
  (add-hook 'typescript-tsx-mode-hook 'flyspell-prog-mode)
  (add-hook 'typescript-tsx-mode-hook (lambda () (setq web-mode-enable-auto-quoting nil)))

  (global-prettify-symbols-mode +1)
  (setq neo-vc-integration '(face))
  (setq neo-theme 'icons)
  ;; (doom-themes-neotree-config)
  (doom-themes-org-config)
  (doom-themes-visual-bell-config)

  (with-eval-after-load 'org
    (defun my/org-mode-hook ()
      (set-face-attribute 'org-level-1 nil :height 1.0)
      (doom-themes-org-config))
    (add-hook 'org-load-hook #'my/org-mode-hook)
    (setq org-adapt-indentation 'headline-data)
    (setq org-todo-keywords '(
                              (sequence "TODO(t)" "IN PROGRESS(p)" "REVIEW(r)" "QA(q)" "|" "DONE(n)")
                              (sequence "TODO(t)" "IN PROGRESS(p)" "WAITING(w)" "|" "DONE(n)" "CANCELLED(c)")))
    (setq org-agenda-custom-commands
          '(("o" "At the office" ((tags-todo "@office" ((org-agenda-span 'day)
                                                        (org-agenda-overriding-header "Office")
                                                        (org-tags-match-list-sublevels nil)))
                                  (agenda "" ((org-agenda-tag-filter-preset '("+@office"))))))
            ;; ("p" "Private" alltodo ""
            ;;  ((org-agenda-files
            ;;    '("~/.gtd/inbox.org.gpg"
            ;;      "~/.gtd/gtd.org.gpg"
            ;;      "~/.gtd/tickler.org.gpg"))))
            ))
    (setq org-agenda-files
          (quote
           ("~/org-roam/")))

    (setq org-capture-templates
          (quote
           (
            ("t" "Todo [inbox]" entry (file+headline "~/.gtd/inbox.org" "Aufgaben") "* TODO %i%?")
            ("o" "Office Task [gtd]" entry (file "~/.gtd/gtd.org") "* TODO %i%?  :@office:")
            ("T" "Tickler" entry (file+headline "~/.gtd/tickler.org" "Tickler") "* %i%? \n %U")
            ("p" "Private Todo [inbox]" entry (file+headline "~/.gtd/inbox.org.gpg" "Aufgaben") "* TODO %i%?")
            ("P" "Private Tickler" entry (file+headline "~/.gtd/tickler.org.gpg" "Tickler") "* %i%? \n %U")
            ("n" "Note" entry (file "~/.gtd/notes.org") "* %?")
            )))

    ;; (add-hook 'org-capture-mode-hook #'org-align-all-tags)
    (setq org-outline-path-complete-in-steps nil)
    (setq org-refile-targets (quote (
                                     ("~/.gtd/gtd.org" :maxlevel . 3)
                                     ("~/.gtd/someday.org" :level . 1)
                                     ("~/.gtd/tickler.org" :maxlevel . 2)
                                     ("~/.gtd/notes.org" :maxlevel . 2)
                                     (org-agenda-files :maxlevel . 9)
                                     )))
    (setq org-refile-use-outline-path (quote buffer-name))
    ;; crypt
    (require 'org-crypt)
    (org-crypt-use-before-save-magic)
    (setq org-tags-exclude-from-inheritance (quote ("crypt")))
    (setq org-crypt-disable-auto-save (quote encrypt))
    ;; GPG key to use for encryption
    ;; Either the Key ID or set to nil to use symmetric encryption.
    (setq org-crypt-key "B63C48A78B4C4A6C")
    ;; (setq org-crypt-key "B9AA48F3F7668FB4C1081444754FDD25C1533026") mac
    (setq org-modules (quote (org-crypt)))
    ;; (setq org-journal-file-format "%Y%m")
    (setq org-agenda-clockreport-parameter-plist
          '(:link t :maxlevel 4 :fileskip0 t :stepskip0 nil :lang de
                  :hidefiles t :properties ("JIRA_TICKET")))

    (setq org-duration-units
          `(("min" . 1)
            ("h" . 60)
            ("d" . ,(* 60 8))
            ("w" . ,(* 60 8 5))
            ("m" . ,(* 60 8 5 4))
            ("y" . ,(* 60 8 5 4 10)))
          )

    )
  (use-package csv-mode
    :init
    (setq csv-separators '(";" "," " ")))

  ;; (spaceline-toggle-all-the-icons-time-off)
  ;; (spaceline-all-the-icons-toggle-slim)
  ;; (use-package spaceline-all-the-icons
  ;;   :after spaceline
  ;;   :config (spaceline-all-the-icons-theme))
  ;; (setq-default spaceline-all-the-icons-separator-type 'none)
  ;; (spacemacs/load-theme 'doom-nova) ; not needed with dev branch
  (require 'org)
  (require 'helm-bookmark)
  ;; (push '("melpa-stable" . "stable.melpa.org/packages/") configuration-layer--elpa-archives)
  ;; (push '(helm . "melpa-stable") package-pinned-packages)
  ;; (spaceline-toggle-all-the-icons-hud-off)
  ;; (spaceline-all-the-icons-toggle-slim)

  ;; (setq flycheck-php-phpcs-executable "~/.composer/vendor/bin/phpcs")

  (add-hook 'js2-mode-hook (lambda () (flyspell-mode-off)))
  (global-set-key (kbd "C-c w w") 'whitespace-mode)
  (global-set-key (kbd "C-c w c") 'whitespace-cleanup)
  (global-unset-key (kbd "C-z"))

  (defun file-notify-rm-all-watches ()
    "Remove all existing file notification watches from Emacs."
    (interactive)
    (maphash
     (lambda (key _value)
       (file-notify-rm-watch key))
     file-notify-descriptors))

  (spacemacs/set-leader-keys "fw" 'file-notify-rm-all-watches)

  (defun helm-buffer-max-length-nil ()
    (interactive)
    (setq helm-buffer-max-length nil)
    )

  (spacemacs/set-leader-keys "bN" 'helm-buffer-max-length-nil)

  (defun bb/setup-term-mode ()
    (evil-local-set-key 'insert (kbd "C-r") 'bb/send-C-r))

  (defun bb/send-C-r ()
    (interactive)
    (term-send-raw-string "\C-r"))

  (add-hook 'term-mode-hook 'bb/setup-term-mode)
  ;; (spacemacs/toggle-mode-line-minor-modes-off)
  ;; (spacemacs/toggle-mode-line-version-control-off)
  ;; (spacemacs/toggle-mode-line-org-clock-on)
  ;; (global-evil-mc-mode 1)
  ;; Puml conf
  (add-to-list 'auto-mode-alist '("\\.puml\\'" . puml-mode))
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . puml-mode))
  (customize-set-variable 'puml-plantuml-jar-path (concat dotspacemacs-directory "plantuml.jar"))
  (add-hook 'org-pomodoro-started-hook
            (lambda ()
              (notifications-notify
               :title "Pomodoro"
               :body "Pomodoro started")))
  (add-hook 'org-pomodoro-finished-hook
            (lambda ()
              (notifications-notify
               :title "Pomodoro"
               :body "Pomodoro completed! \nTime for a break."
               :timeout 0)))
  (add-hook 'org-pomodoro-break-finished-hook
            (lambda ()
              (notifications-notify
               :title "Pomodoro"
               :body "Pomodoro Short Break Finished \nReady for Another?"
               :timeout 0)))
  (add-hook 'org-pomodoro-long-break-finished-hook
            (lambda ()
              (notifications-notify
               :title "Pomodoro"
               :body "Pomodoro Long Break Finished \nReady for Another?"
               :timeout 0)))
  (add-hook 'org-pomodoro-killed-hook
            (lambda ()
              (notifications-notify
               :title "Pomodoro"
               :body "Pomodoro Killed!\nOne does not simply kill a pomodoro!"
               :timeout 0)))
  (add-to-list 'auto-mode-alist '("\\.vm\\'" . web-mode))
  (setq web-mode-engines-alist '(("velocity"    . "\\.vm\\'")))

  (setq org-roam-capture-templates
        '(("d" "default" plain "%?"
           :if-new (file+head "${slug}.org" "#+title: ${title}\n")
           :unnarrowed t)))

  (defun eslint-fix-file ()
    (interactive)
    (message "eslint --fixing the file" (buffer-file-name))
    (call-process "eslint" nil t nil "--fix" buffer-file-name))

  (defun eslint-fix-file-and-revert ()
    (interactive)
    (eslint-fix-file)
    (revert-buffer t t))

  (spacemacs/toggle-transparency)
  ;; (add-hook 'js2-mode-hook
  ;;           (lambda ()
  ;;             (add-hook 'after-save-hook #'eslint-fix-file-and-revert)))
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
